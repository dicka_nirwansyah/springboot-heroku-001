package com.dicka.heroku.springbootheroku.controller;

import com.dicka.heroku.springbootheroku.dao.BookDao;
import com.dicka.heroku.springbootheroku.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/book")
public class ControllerBook {

    @Autowired
    private BookDao bookDao;

    @GetMapping
    public ResponseEntity<List<Book>> listBook(){
        return Optional.ofNullable(bookDao.listBook())
                .map(resultset -> new ResponseEntity<>(resultset, HttpStatus.OK))
                .orElse(new ResponseEntity<List<Book>>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = "/{idbuku}")
    public ResponseEntity<Book> getId(@PathVariable String idbuku){
        return Optional.ofNullable(bookDao.getId(idbuku))
                .map(resultset -> new ResponseEntity<>(resultset, HttpStatus.OK))
                .orElse(new ResponseEntity<Book>(HttpStatus.BAD_REQUEST));
    }

    @PostMapping(value = "/created")
    public ResponseEntity<Book> created(@RequestBody Book book){
        return Optional.ofNullable(bookDao.createdBook(book))
                .map(resultset -> new ResponseEntity<>(resultset, HttpStatus.OK))
                .orElse(new ResponseEntity<Book>(HttpStatus.BAD_REQUEST));
    }
}
