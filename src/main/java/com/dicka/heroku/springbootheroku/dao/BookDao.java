package com.dicka.heroku.springbootheroku.dao;

import com.dicka.heroku.springbootheroku.entity.Book;

import java.util.List;

public interface BookDao {

    List<Book> listBook();

    Book createdBook(Book book);

    Book getId(String idbuku);
}
