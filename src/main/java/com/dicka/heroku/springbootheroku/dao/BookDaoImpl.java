package com.dicka.heroku.springbootheroku.dao;

import com.dicka.heroku.springbootheroku.entity.Book;
import com.dicka.heroku.springbootheroku.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Repository
@Transactional
public class BookDaoImpl implements BookDao{

    @Autowired
    private BookRepo bookRepo;

    @Override
    public List<Book> listBook() {
        List<Book> books = new ArrayList<>();
        for(Book book : bookRepo.findAll()){
            books.add(book);
        }
        return books;
    }

    @Override
    public Book createdBook(Book book) {
        if(book.getIdbuku() != null){
            bookRepo.save(book);
        }else{
            bookRepo.save(book);
        }
        return book;
    }

    @Override
    public Book getId(String idbuku) {
        return bookRepo.findOne(idbuku);
    }
}
