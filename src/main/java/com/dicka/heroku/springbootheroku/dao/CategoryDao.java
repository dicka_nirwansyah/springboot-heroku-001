package com.dicka.heroku.springbootheroku.dao;

import com.dicka.heroku.springbootheroku.entity.Category;

import java.util.List;

public interface CategoryDao {

    List<Category> listCategory();

    Category getId(String idcategory);

    Category created(Category category);
}
