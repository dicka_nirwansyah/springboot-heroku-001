package com.dicka.heroku.springbootheroku.dao;

import com.dicka.heroku.springbootheroku.entity.Category;
import com.dicka.heroku.springbootheroku.repo.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
@Service
public class CategoryDaoImpl implements CategoryDao{

    @Autowired
    private CategoryRepo categoryRepo;

    @Override
    public List<Category> listCategory() {
        return categoryRepo.findAll();
    }

    @Override
    public Category getId(String idcategory) {
        return categoryRepo.findOne(idcategory);
    }

    @Override
    public Category created(Category category) {
        if(category.getIdcategory() != null){
            categoryRepo.save(category);
        }else{
            categoryRepo.save(category);
        }
        return category;
    }
}
