package com.dicka.heroku.springbootheroku.repo;

import com.dicka.heroku.springbootheroku.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepo extends JpaRepository<Book, String>{
}
