package com.dicka.heroku.springbootheroku.repo;

import com.dicka.heroku.springbootheroku.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepo extends JpaRepository<Category, String>{
}
