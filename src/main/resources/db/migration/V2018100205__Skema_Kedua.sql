create table book(
idbuku varchar(255) not null,
title varchar(255) not null,
author varchar(255) not null,
price int not null,
quantity int not null,
disabled boolean not null,
idcategory varchar(255) not null,

constraint pk_book_idbuku primary key(idbuku),
constraint fk_book_idcategory foreign key(idcategory)
references category(idcategory)
);